import './style.scss'
import { init } from './components/helpers/init'
import { isAuth } from './components/auth'

init()

const app = `
  <main class="Gram">
    ${isAuth()}
  </main>
`

document.getElementById('root').innerHTML = app

