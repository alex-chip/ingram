import header from './header'
import timeline from './timeline'
import camera from './camera'
import uploader from './uploader'
import profile from './profile'
import footer from './footer'

const app = () => (`
  ${header()}
  <section class="Content">
    ${profile()}
    ${timeline()}
    ${camera()}
    ${uploader()}
  </section>
  ${footer()}
`)

export default app
