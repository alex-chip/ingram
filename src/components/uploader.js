import firebase from 'firebase'
import { progressBar, showProgress, progressStatus } from './upload_progress'
import { errorMsg, successMsg } from './helpers/messages'

const uploader = () => {
  const d = document
  const c = console.log

  const uploaderScripts = setInterval(() => {
    if (d.readyState === 'complete') {
      clearInterval(uploaderScripts)

      const storageRef = firebase.storage().ref().child('photos')
      const dbRef = firebase.database().ref().child('photos')
      const user = firebase.auth().currentUser
      const uploader = d.getElementById('uploader')
      const form = d.getElementById('upload')
      const outputMessage = d.querySelector('.Uploader').querySelector('.Progress-output')

      uploader.addEventListener('change', e => {
        outputMessage.innerHTML = ''
        Array.from(e.target.files).forEach(file => {
          if (file.type.match('image.*')) {
            let uploadTask = storageRef.child(file.name).put(file)

            uploadTask.on('state_changed', data => {
              showProgress()
              progressStatus(data)
            }, err => {
              c(err, err.code, err.message)
              outputMessage.innerHTML = errorMsg(`${err.message}`, err)
            }, () => {
              storageRef.child(file.name).getDownloadURL()
                .then(url => {
                  outputMessage.insertAdjacentHTML(
                    'afterbegin',
                    `${successMsg('Tu imagen se ha subido')} <img src="${url}">`
                    )
                })
                .catch(err => { outputMessage.innerHTML = errorMsg(`${err.message}`, err) })
            })
          } else {
            outputMessage.innerHTML = errorMsg('Tu archivo debe ser una imagen', null)
          }
        })
        form.reset()
      })
    }
  }, 100)
  return `
    <article class="Uploader Content-section u-show">
      <h2 class="u-title">Sube tus Fotos</h2>
      <form name="upload" id="upload">
        <input type="file" id="uploader" multiple/>
        <label for="uploader">
          <i class="fa fa-cloud-upload" title="Sube tu foto"></i>
        </label>
      </form>
      ${progressBar()}
    </article>
  `
}

export default uploader
