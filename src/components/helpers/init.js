import firebase from 'firebase'

  // Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyAhzl2-J2X9gnjjOS9v9L7GXaPqkfQ2VXI',
  authDomain: 'alex-gram.firebaseapp.com',
  databaseURL: 'https://alex-gram.firebaseio.com',
  projectId: 'alex-gram',
  storageBucket: 'alex-gram.appspot.com',
  messagingSenderId: '315859187787',
  appId: '1:315859187787:web:5036afd5f0b6f814'
}
// Initialize Firebase
export const init = () => firebase.initializeApp(firebaseConfig)
