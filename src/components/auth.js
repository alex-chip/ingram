import firebase from 'firebase'
import app from './app'

const d = document
const c = console.log

const githubSignIn = () => {
  const provider = new firebase.auth.GithubAuthProvider()
  firebase.auth().signInWithPopup(provider)
    .then(result => c(`${result.user.email} ha iniciado sesion con GitHub`, result))
    .catch(err => c(`Error ${err.code}: ${err.message}`))
}

const githubSignOut = () => {
  firebase.auth().signOut()
    .then(() => c('Te has desconectado correctamente de GitHub'))
    .catch(() => c('Ha ocurrido un error'))
}

const signIn = () => {
  d.addEventListener('click', e => {
    if (e.target.matches('.Sign-button')) {
      githubSignIn()
    }
  })
  return `
    <div class="Sign">
      <h1 class="Sign-title">InGram</h1>
      <button class="Sign-button">
        <i class="fa fa-sign-in"></i>
        entra con
        <i class="fa fa-github"></i>
      </button>
    </div>
  `
}

export const signOut = () => {
  d.addEventListener('click', e => {
    if (e.target.matches('.logout')) {
      githubSignOut()
    }
  })
  return `
    <button title="Salir">
      <i class="logout fa fa-sign-out"></i>
    </button>
  `
}

export const isAuth = () => {
  firebase.auth().onAuthStateChanged(user => {
    const InGram = d.querySelector('.Gram')

    if (user) {
      InGram.innerHTML = app()
      InGram.classList.add('u-jc-flex-start')
      // c('Usuario Autenticado')
    } else {
      InGram.innerHTML = signIn()
      InGram.classList.remove('u-jc-flex-start')
      // c('Usuario NO Autenticado')
    }
  })
}
